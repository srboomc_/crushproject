﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void HealthBarController::Start()
extern void HealthBarController_Start_m02B852C32CC6B9FA058E5BB444A78C1125E3F092 ();
// 0x00000002 System.Void HealthBarController::UpdateHeartsHUD()
extern void HealthBarController_UpdateHeartsHUD_m7CA6A8380F38CB7C5DED7C97F2C46CACF91A23C7 ();
// 0x00000003 System.Void HealthBarController::SetHeartContainers()
extern void HealthBarController_SetHeartContainers_m33C0F74C857AF845A7EAE7D0BF57B684FAB49FF9 ();
// 0x00000004 System.Void HealthBarController::SetFilledHearts()
extern void HealthBarController_SetFilledHearts_m5C93EBF0B73FDA16E5384B8393E30A7D11D45187 ();
// 0x00000005 System.Void HealthBarController::InstantiateHeartContainers()
extern void HealthBarController_InstantiateHeartContainers_mCF86532B8FD330544D2CF3D9C29B46A2EE958096 ();
// 0x00000006 System.Void HealthBarController::.ctor()
extern void HealthBarController__ctor_m0DA81F52C571BA3F07D8813F6E58D885DEE822A9 ();
// 0x00000007 System.Void HealthBarHUDTester::AddHealth()
extern void HealthBarHUDTester_AddHealth_m34CA23A95115BECE258FEB2A816C7026D267B41E ();
// 0x00000008 System.Void HealthBarHUDTester::Heal(System.Single)
extern void HealthBarHUDTester_Heal_m4DA505B61B61D5853995E8BFD7E0A115C108A7AB ();
// 0x00000009 System.Void HealthBarHUDTester::Hurt(System.Single)
extern void HealthBarHUDTester_Hurt_m2148E4685171C9B3D6252719AC53C37C49836C28 ();
// 0x0000000A System.Void HealthBarHUDTester::.ctor()
extern void HealthBarHUDTester__ctor_mEA260E66A07A03867AB78003B1F1B9C1F6BB1E64 ();
// 0x0000000B PlayerStats PlayerStats::get_Instance()
extern void PlayerStats_get_Instance_m636F80EF5A82000E19F91BB0203914094DCB5460 ();
// 0x0000000C System.Single PlayerStats::get_Health()
extern void PlayerStats_get_Health_m98A3CA6DFD959CA2C03F1A4482C90ED695FB4E2F ();
// 0x0000000D System.Single PlayerStats::get_MaxHealth()
extern void PlayerStats_get_MaxHealth_m32FBF750484D0BE0152D4F6504935E226A705DCA ();
// 0x0000000E System.Single PlayerStats::get_MaxTotalHealth()
extern void PlayerStats_get_MaxTotalHealth_mC0C819446B64F5C2F1AE4B590217E295756B8DB9 ();
// 0x0000000F System.Void PlayerStats::Heal(System.Single)
extern void PlayerStats_Heal_mDB3893E945F45ED05B30D0C3124486B074958583 ();
// 0x00000010 System.Void PlayerStats::TakeDamage(System.Single)
extern void PlayerStats_TakeDamage_m3CCE6B4662B4801B657BA90FFBD3535A8295DAB8 ();
// 0x00000011 System.Void PlayerStats::AddHealth()
extern void PlayerStats_AddHealth_mB8B2A4C868A1B26A7E315994CCF4A0ED319FF592 ();
// 0x00000012 System.Void PlayerStats::ClampHealth()
extern void PlayerStats_ClampHealth_m9FE143FA841D802EC463B7DC70E3CC23F47179C7 ();
// 0x00000013 System.Void PlayerStats::.ctor()
extern void PlayerStats__ctor_m3634C01B2ED65E8E99697A2289A21BDA8A6DCB8A ();
// 0x00000014 System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m16EE5DC9112525F5AAEB9792FFEE244B522B5587 ();
// 0x00000015 System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m89D545030FDA9A59A20B9519D3C374F4273A6524 ();
// 0x00000016 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m715EF85B92C3E1E233D3D1ECF5ACA5EFC245770E ();
// 0x00000017 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m91DFB04F3304E63D2ABC280EB25B769600EA07C7 ();
// 0x00000018 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_mF24FD58237B87D454B3CC3F4319B39C4ACAD865F ();
// 0x00000019 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m2D14225CCA2E188564E168A73B21870A9FF430F8 ();
// 0x0000001A System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m140CF6C4E97F01B6A6C36DDBEDD055BF52440918 ();
// 0x0000001B System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_mB9C5F2E07F3DACB63437014AC49FB2E43A93BB97 ();
// 0x0000001C System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m14DCFA3E0E45D44D17B9D702FC61CCBA16865C87 ();
// 0x0000001D System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_mDACE2A5D3A6FB7D4D8EEF188DE474D2BEC1A5668 ();
// 0x0000001E UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_mF64961ED5359C8E31E79CAA306019CB66DF50F3E ();
// 0x0000001F System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_m4822883A26EE341755D07B8AE12144E91BFF4E65 ();
// 0x00000020 System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m209C3CF78BC69137D3BCBC16BC2291303DC0D044 ();
// 0x00000021 System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_m1C7BD962FC0F779D4DF910BB79CB73608CEF40E2 ();
// 0x00000022 System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_m46F42EE1552C943114901EE8F9E5BD88AE5881C5 ();
// 0x00000023 AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_m41669BF41810BA976B1A230E1FB3ADCDC1F4C758 ();
// 0x00000024 System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m74C085FEB45B977EAB77D952739324ED0D82CA99 ();
// 0x00000025 System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m1D5DFFE729C0815F13B5E3FEF40368F495E2281F ();
// 0x00000026 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_m919DE6645E9AC3F5CA9D709A407AC25F941AD43D ();
// 0x00000027 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_mEE1EC80F072F8664263B7CD670B9CDFD5A7DA4D8 ();
// 0x00000028 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_mDB23A762AE197D25D02CC22EDD628ED74CB63240 ();
// 0x00000029 System.Void Joystick::Start()
extern void Joystick_Start_mC98BE8B1D2D3B108F8757013FDF125D9585FCD16 ();
// 0x0000002A System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m2DD4D088DFCF58CFEEDD7F6A4AB3C4634123916E ();
// 0x0000002B System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_mAACE365E25C4D7D85229744914EB3BBE24A51520 ();
// 0x0000002C System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m006268D0ECEDBC64945069D24185AE856A2D53E4 ();
// 0x0000002D System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mE42BC60075703BD6A5FD66D8E8DE9C4D9ED26D4D ();
// 0x0000002E System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mCCB760E6DE72CA6A3E409CC28C3C4E7DCBCA8F2A ();
// 0x0000002F System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m6C84220D9925906C4BF7116F2E4C1F8538307691 ();
// 0x00000030 UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mE4C429E76D0FA78FD1567EB1361AF68141706201 ();
// 0x00000031 System.Void Joystick::.ctor()
extern void Joystick__ctor_m95B949C8261D82D8BF39CBF8FB0FE5C8B8BE5CF6 ();
// 0x00000032 System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m0856D72D5E183004124179B1D3AD8CAD7351C006 ();
// 0x00000033 System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_mDC1C60A5373D048BF13B0145603C45C5F7DC50DA ();
// 0x00000034 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_m8D013E6B2035898BC05B942A2C1762799540F0EF ();
// 0x00000035 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_m48EC5D21D865265F3DCF004D6C9D4A8B4EF97EA9 ();
// 0x00000036 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m1DC93EE3505D0A77ECB5849F99F50EA418B28C30 ();
// 0x00000037 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_mD08EF9AE72DE3D48292A7288EAA0FF896849C5C7 ();
// 0x00000038 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_mC36FAC17C0AB963374E74F8A06B875717901AF45 ();
// 0x00000039 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_mA61B63185505CDC0D30DC9D3CDCC7A5F3DEC5649 ();
// 0x0000003A System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_m395AD17E93B4E21B0964EEC6A7D894F27AE96032 ();
// 0x0000003B System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_m6B39A5B8CB4EC42840A54C1B7B4C7B4762221470 ();
// 0x0000003C System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m1540371430584B05B7EC121C42D2E92F256C6151 ();
// 0x0000003D System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_mC3CE268EB3B6CFA9CCEA41910DBA2360AC8C673F ();
// 0x0000003E System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m41DC949EE02863CA514BFFA25BEFD0B0506EF37D ();
// 0x0000003F System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_mEF208735B031A113821948192AF521BBCFD7CB31 ();
// 0x00000040 System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_m40D937E2836CD2C966B3A5F5BDBDD3D9CD0DD0EC ();
// 0x00000041 System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m386A0D7C8A118274A91C06A132F90D41B03DE448 ();
// 0x00000042 System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m069D861ED066C32DF6870402B20DF407AA230A0F ();
// 0x00000043 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m26066C0F89DEC5BC94E0B2B22ACC9F946E896EC8 ();
// 0x00000044 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_mA5A9E0F283E12ADEAB49675E14C4B5D3D67BCE5E ();
// 0x00000045 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_mBDBBA4075577E1DA4610BC58C7DBDD5BCC4104FF ();
// 0x00000046 System.Void CameraLogic::Start()
extern void CameraLogic_Start_m90BD8B59BA53C54DA5C0FBF0EDBF487E79FDCE94 ();
// 0x00000047 System.Void CameraLogic::SwitchTarget(System.Int32)
extern void CameraLogic_SwitchTarget_m220D2F952A166CB76F77DD1697C39E4E43508635 ();
// 0x00000048 System.Void CameraLogic::NextTarget()
extern void CameraLogic_NextTarget_mC32B488BF41618E7610269DFD943498E48169AB0 ();
// 0x00000049 System.Void CameraLogic::PreviousTarget()
extern void CameraLogic_PreviousTarget_m4E8F6FA319F3527C858B810A5487AC915494D398 ();
// 0x0000004A System.Void CameraLogic::Update()
extern void CameraLogic_Update_m601C2BA8E1AE645B841CD4F91F117BF4560C5FAC ();
// 0x0000004B System.Void CameraLogic::LateUpdate()
extern void CameraLogic_LateUpdate_m91CCE3C50BEDC3764202E163238A12A00F5C2EFC ();
// 0x0000004C System.Void CameraLogic::.ctor()
extern void CameraLogic__ctor_m2CF4E2D15B46451FB28B2D44E5CD7870C31C3937 ();
// 0x0000004D System.Void Demo::Start()
extern void Demo_Start_m637ECDD592EB56E50DF5DC31BAB58CA4012187CC ();
// 0x0000004E System.Void Demo::Update()
extern void Demo_Update_m65F0DD7CAFCDBBCBB8FE10E810E798EDE812FD91 ();
// 0x0000004F System.Void Demo::OnGUI()
extern void Demo_OnGUI_mAAA9F9D7A772B48374B2DAF6DAEB77F864ECDD0A ();
// 0x00000050 System.Void Demo::.ctor()
extern void Demo__ctor_m83B824A1692A6ACC2A39D8630D7138F8FEA2EDB2 ();
// 0x00000051 System.Void SimpleCharacterControl::OnCollisionEnter(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionEnter_m7632A46485EBECE850A347F48BEE8356A86BB9FF ();
// 0x00000052 System.Void SimpleCharacterControl::OnCollisionStay(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionStay_m892ABF3421FC60F31622503EC83AFBD09B2E5772 ();
// 0x00000053 System.Void SimpleCharacterControl::OnCollisionExit(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionExit_m498C5FAE2A486391211D2B92B1C52E5D145164C7 ();
// 0x00000054 System.Void SimpleCharacterControl::Update()
extern void SimpleCharacterControl_Update_mE336B635E0C51B4F6CD8382444E39C406E29C2FA ();
// 0x00000055 System.Void SimpleCharacterControl::TankUpdate()
extern void SimpleCharacterControl_TankUpdate_m6F2AC1417C5BA2D7EE2291D58672C06FA1FFD205 ();
// 0x00000056 System.Void SimpleCharacterControl::DirectUpdate()
extern void SimpleCharacterControl_DirectUpdate_mEB946D2FAAAFE0C9C581519374700A6EB6D832E7 ();
// 0x00000057 System.Void SimpleCharacterControl::JumpingAndLanding()
extern void SimpleCharacterControl_JumpingAndLanding_m1E22B98A3A74307D058A0F0BC432328E8A169B6E ();
// 0x00000058 System.Void SimpleCharacterControl::.ctor()
extern void SimpleCharacterControl__ctor_m1D1E2D16674FCFB0C10A4DB4856A7DDEDD019857 ();
// 0x00000059 System.Void qsScript::playGame1()
extern void qsScript_playGame1_m1F29FE82045A00933C5E4A937DA0887A186ACBE6 ();
// 0x0000005A System.Void qsScript::playGame2()
extern void qsScript_playGame2_mAF67BD74EAB249688ABC0D4CEADF6FA42BDB7481 ();
// 0x0000005B System.Void qsScript::playGame3()
extern void qsScript_playGame3_m0287043EEE996B6719D6AB090597E88E5D046A39 ();
// 0x0000005C System.Void qsScript::.ctor()
extern void qsScript__ctor_m17B2C3690D0FAA62B62EA87C97CB04792411AAC4 ();
// 0x0000005D System.Void TapObject::Start()
extern void TapObject_Start_mA5FF65690991ADF170BDA001869EFBBB04DACB40 ();
// 0x0000005E System.Void TapObject::Update()
extern void TapObject_Update_m8EFDF49DBF92542CC62EB880AACBBD474B2A27C3 ();
// 0x0000005F System.Void TapObject::UpdatePlacementPose()
extern void TapObject_UpdatePlacementPose_m2B01C199B32209A905183C00218FAC7673CEEABF ();
// 0x00000060 System.Void TapObject::.ctor()
extern void TapObject__ctor_m03362C2439EF138FC96B734792F8B026D22642A6 ();
// 0x00000061 System.Void maleMove::Start()
extern void maleMove_Start_mBB3812559561A577E6E7236CE4E92BCA2C71CE15 ();
// 0x00000062 System.Void maleMove::Update()
extern void maleMove_Update_m8E71103445BF3B69264D55134AA99958E958839A ();
// 0x00000063 System.Void maleMove::OnTriggerEnter(UnityEngine.Collider)
extern void maleMove_OnTriggerEnter_m2EBC1BE40BD093F3050C9C49499CB99076A00DC7 ();
// 0x00000064 System.Collections.IEnumerator maleMove::Wait()
extern void maleMove_Wait_m126A6AA08CFF1A849029A9F9FF421A18A17795F0 ();
// 0x00000065 System.Void maleMove::sadScene()
extern void maleMove_sadScene_m93C58647198AF096D17DA921BC74C709CCE486EC ();
// 0x00000066 System.Void maleMove::qScene()
extern void maleMove_qScene_mA3135120CB6ED156A57915E60EEAE18AF32FEC90 ();
// 0x00000067 System.Void maleMove::AddHealth()
extern void maleMove_AddHealth_mC934C5FD23528F1A7A8CE90907FE5E4B1693DED8 ();
// 0x00000068 System.Void maleMove::Heal(System.Single)
extern void maleMove_Heal_mD9CF89580C70E3F0156BC51E40D45C73245B617C ();
// 0x00000069 System.Void maleMove::Hurt(System.Single)
extern void maleMove_Hurt_m7AE8D082443338702D5A1B1B83FFA5B212DF423B ();
// 0x0000006A System.Void maleMove::_Heal()
extern void maleMove__Heal_m1C9A7E40CCF6CA2ED2765157598784106320BD10 ();
// 0x0000006B System.Void maleMove::_Hurt()
extern void maleMove__Hurt_mEEFF23AE24B5E3E2569628CE7A1A74FF0BCB4DEF ();
// 0x0000006C System.Void maleMove::updateTextMine(System.Int32,System.Int32)
extern void maleMove_updateTextMine_mEC9B6972326A37B47ED546BE5F761B75647BC9C8 ();
// 0x0000006D System.Void maleMove::.ctor()
extern void maleMove__ctor_m3DE00F53A727BF826E43BA20F5816BC2B0B047E0 ();
// 0x0000006E System.Void PlayerStats_OnHealthChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnHealthChangedDelegate__ctor_m3CF0F0556DE93BD37C78A360602129F13F6B1B5F ();
// 0x0000006F System.Void PlayerStats_OnHealthChangedDelegate::Invoke()
extern void OnHealthChangedDelegate_Invoke_m6BFDB0DD8B980387D188C2F48B396C62F1BDA633 ();
// 0x00000070 System.IAsyncResult PlayerStats_OnHealthChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnHealthChangedDelegate_BeginInvoke_mC27003B190C1E8B9F95574201198B56AD954F347 ();
// 0x00000071 System.Void PlayerStats_OnHealthChangedDelegate::EndInvoke(System.IAsyncResult)
extern void OnHealthChangedDelegate_EndInvoke_m6F94BF78376F371D033033DC7152B15E0F7F86A1 ();
// 0x00000072 System.Void maleMove_<Wait>d__18::.ctor(System.Int32)
extern void U3CWaitU3Ed__18__ctor_m7260307B18BDC5D64D4CF6803CDC97CA59C6403D ();
// 0x00000073 System.Void maleMove_<Wait>d__18::System.IDisposable.Dispose()
extern void U3CWaitU3Ed__18_System_IDisposable_Dispose_mDE45B6C5AC3D81F040597555B42674E7705C47B1 ();
// 0x00000074 System.Boolean maleMove_<Wait>d__18::MoveNext()
extern void U3CWaitU3Ed__18_MoveNext_m438946F3661F9CEE0D5A845D7E18BCAE14528206 ();
// 0x00000075 System.Object maleMove_<Wait>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31769C4CA946C96D0D04FF74C528C93E6F7150EF ();
// 0x00000076 System.Void maleMove_<Wait>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_m096B348A157986A020DF0164730877DFC427BE71 ();
// 0x00000077 System.Object maleMove_<Wait>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWaitU3Ed__18_System_Collections_IEnumerator_get_Current_m8887D5CC0BC8989FFE458D54A4B0057126278A30 ();
static Il2CppMethodPointer s_methodPointers[119] = 
{
	HealthBarController_Start_m02B852C32CC6B9FA058E5BB444A78C1125E3F092,
	HealthBarController_UpdateHeartsHUD_m7CA6A8380F38CB7C5DED7C97F2C46CACF91A23C7,
	HealthBarController_SetHeartContainers_m33C0F74C857AF845A7EAE7D0BF57B684FAB49FF9,
	HealthBarController_SetFilledHearts_m5C93EBF0B73FDA16E5384B8393E30A7D11D45187,
	HealthBarController_InstantiateHeartContainers_mCF86532B8FD330544D2CF3D9C29B46A2EE958096,
	HealthBarController__ctor_m0DA81F52C571BA3F07D8813F6E58D885DEE822A9,
	HealthBarHUDTester_AddHealth_m34CA23A95115BECE258FEB2A816C7026D267B41E,
	HealthBarHUDTester_Heal_m4DA505B61B61D5853995E8BFD7E0A115C108A7AB,
	HealthBarHUDTester_Hurt_m2148E4685171C9B3D6252719AC53C37C49836C28,
	HealthBarHUDTester__ctor_mEA260E66A07A03867AB78003B1F1B9C1F6BB1E64,
	PlayerStats_get_Instance_m636F80EF5A82000E19F91BB0203914094DCB5460,
	PlayerStats_get_Health_m98A3CA6DFD959CA2C03F1A4482C90ED695FB4E2F,
	PlayerStats_get_MaxHealth_m32FBF750484D0BE0152D4F6504935E226A705DCA,
	PlayerStats_get_MaxTotalHealth_mC0C819446B64F5C2F1AE4B590217E295756B8DB9,
	PlayerStats_Heal_mDB3893E945F45ED05B30D0C3124486B074958583,
	PlayerStats_TakeDamage_m3CCE6B4662B4801B657BA90FFBD3535A8295DAB8,
	PlayerStats_AddHealth_mB8B2A4C868A1B26A7E315994CCF4A0ED319FF592,
	PlayerStats_ClampHealth_m9FE143FA841D802EC463B7DC70E3CC23F47179C7,
	PlayerStats__ctor_m3634C01B2ED65E8E99697A2289A21BDA8A6DCB8A,
	JoystickPlayerExample_FixedUpdate_m16EE5DC9112525F5AAEB9792FFEE244B522B5587,
	JoystickPlayerExample__ctor_m89D545030FDA9A59A20B9519D3C374F4273A6524,
	JoystickSetterExample_ModeChanged_m715EF85B92C3E1E233D3D1ECF5ACA5EFC245770E,
	JoystickSetterExample_AxisChanged_m91DFB04F3304E63D2ABC280EB25B769600EA07C7,
	JoystickSetterExample_SnapX_mF24FD58237B87D454B3CC3F4319B39C4ACAD865F,
	JoystickSetterExample_SnapY_m2D14225CCA2E188564E168A73B21870A9FF430F8,
	JoystickSetterExample_Update_m140CF6C4E97F01B6A6C36DDBEDD055BF52440918,
	JoystickSetterExample__ctor_mB9C5F2E07F3DACB63437014AC49FB2E43A93BB97,
	Joystick_get_Horizontal_m14DCFA3E0E45D44D17B9D702FC61CCBA16865C87,
	Joystick_get_Vertical_mDACE2A5D3A6FB7D4D8EEF188DE474D2BEC1A5668,
	Joystick_get_Direction_mF64961ED5359C8E31E79CAA306019CB66DF50F3E,
	Joystick_get_HandleRange_m4822883A26EE341755D07B8AE12144E91BFF4E65,
	Joystick_set_HandleRange_m209C3CF78BC69137D3BCBC16BC2291303DC0D044,
	Joystick_get_DeadZone_m1C7BD962FC0F779D4DF910BB79CB73608CEF40E2,
	Joystick_set_DeadZone_m46F42EE1552C943114901EE8F9E5BD88AE5881C5,
	Joystick_get_AxisOptions_m41669BF41810BA976B1A230E1FB3ADCDC1F4C758,
	Joystick_set_AxisOptions_m74C085FEB45B977EAB77D952739324ED0D82CA99,
	Joystick_get_SnapX_m1D5DFFE729C0815F13B5E3FEF40368F495E2281F,
	Joystick_set_SnapX_m919DE6645E9AC3F5CA9D709A407AC25F941AD43D,
	Joystick_get_SnapY_mEE1EC80F072F8664263B7CD670B9CDFD5A7DA4D8,
	Joystick_set_SnapY_mDB23A762AE197D25D02CC22EDD628ED74CB63240,
	Joystick_Start_mC98BE8B1D2D3B108F8757013FDF125D9585FCD16,
	Joystick_OnPointerDown_m2DD4D088DFCF58CFEEDD7F6A4AB3C4634123916E,
	Joystick_OnDrag_mAACE365E25C4D7D85229744914EB3BBE24A51520,
	Joystick_HandleInput_m006268D0ECEDBC64945069D24185AE856A2D53E4,
	Joystick_FormatInput_mE42BC60075703BD6A5FD66D8E8DE9C4D9ED26D4D,
	Joystick_SnapFloat_mCCB760E6DE72CA6A3E409CC28C3C4E7DCBCA8F2A,
	Joystick_OnPointerUp_m6C84220D9925906C4BF7116F2E4C1F8538307691,
	Joystick_ScreenPointToAnchoredPosition_mE4C429E76D0FA78FD1567EB1361AF68141706201,
	Joystick__ctor_m95B949C8261D82D8BF39CBF8FB0FE5C8B8BE5CF6,
	DynamicJoystick_get_MoveThreshold_m0856D72D5E183004124179B1D3AD8CAD7351C006,
	DynamicJoystick_set_MoveThreshold_mDC1C60A5373D048BF13B0145603C45C5F7DC50DA,
	DynamicJoystick_Start_m8D013E6B2035898BC05B942A2C1762799540F0EF,
	DynamicJoystick_OnPointerDown_m48EC5D21D865265F3DCF004D6C9D4A8B4EF97EA9,
	DynamicJoystick_OnPointerUp_m1DC93EE3505D0A77ECB5849F99F50EA418B28C30,
	DynamicJoystick_HandleInput_mD08EF9AE72DE3D48292A7288EAA0FF896849C5C7,
	DynamicJoystick__ctor_mC36FAC17C0AB963374E74F8A06B875717901AF45,
	FixedJoystick__ctor_mA61B63185505CDC0D30DC9D3CDCC7A5F3DEC5649,
	FloatingJoystick_Start_m395AD17E93B4E21B0964EEC6A7D894F27AE96032,
	FloatingJoystick_OnPointerDown_m6B39A5B8CB4EC42840A54C1B7B4C7B4762221470,
	FloatingJoystick_OnPointerUp_m1540371430584B05B7EC121C42D2E92F256C6151,
	FloatingJoystick__ctor_mC3CE268EB3B6CFA9CCEA41910DBA2360AC8C673F,
	VariableJoystick_get_MoveThreshold_m41DC949EE02863CA514BFFA25BEFD0B0506EF37D,
	VariableJoystick_set_MoveThreshold_mEF208735B031A113821948192AF521BBCFD7CB31,
	VariableJoystick_SetMode_m40D937E2836CD2C966B3A5F5BDBDD3D9CD0DD0EC,
	VariableJoystick_Start_m386A0D7C8A118274A91C06A132F90D41B03DE448,
	VariableJoystick_OnPointerDown_m069D861ED066C32DF6870402B20DF407AA230A0F,
	VariableJoystick_OnPointerUp_m26066C0F89DEC5BC94E0B2B22ACC9F946E896EC8,
	VariableJoystick_HandleInput_mA5A9E0F283E12ADEAB49675E14C4B5D3D67BCE5E,
	VariableJoystick__ctor_mBDBBA4075577E1DA4610BC58C7DBDD5BCC4104FF,
	CameraLogic_Start_m90BD8B59BA53C54DA5C0FBF0EDBF487E79FDCE94,
	CameraLogic_SwitchTarget_m220D2F952A166CB76F77DD1697C39E4E43508635,
	CameraLogic_NextTarget_mC32B488BF41618E7610269DFD943498E48169AB0,
	CameraLogic_PreviousTarget_m4E8F6FA319F3527C858B810A5487AC915494D398,
	CameraLogic_Update_m601C2BA8E1AE645B841CD4F91F117BF4560C5FAC,
	CameraLogic_LateUpdate_m91CCE3C50BEDC3764202E163238A12A00F5C2EFC,
	CameraLogic__ctor_m2CF4E2D15B46451FB28B2D44E5CD7870C31C3937,
	Demo_Start_m637ECDD592EB56E50DF5DC31BAB58CA4012187CC,
	Demo_Update_m65F0DD7CAFCDBBCBB8FE10E810E798EDE812FD91,
	Demo_OnGUI_mAAA9F9D7A772B48374B2DAF6DAEB77F864ECDD0A,
	Demo__ctor_m83B824A1692A6ACC2A39D8630D7138F8FEA2EDB2,
	SimpleCharacterControl_OnCollisionEnter_m7632A46485EBECE850A347F48BEE8356A86BB9FF,
	SimpleCharacterControl_OnCollisionStay_m892ABF3421FC60F31622503EC83AFBD09B2E5772,
	SimpleCharacterControl_OnCollisionExit_m498C5FAE2A486391211D2B92B1C52E5D145164C7,
	SimpleCharacterControl_Update_mE336B635E0C51B4F6CD8382444E39C406E29C2FA,
	SimpleCharacterControl_TankUpdate_m6F2AC1417C5BA2D7EE2291D58672C06FA1FFD205,
	SimpleCharacterControl_DirectUpdate_mEB946D2FAAAFE0C9C581519374700A6EB6D832E7,
	SimpleCharacterControl_JumpingAndLanding_m1E22B98A3A74307D058A0F0BC432328E8A169B6E,
	SimpleCharacterControl__ctor_m1D1E2D16674FCFB0C10A4DB4856A7DDEDD019857,
	qsScript_playGame1_m1F29FE82045A00933C5E4A937DA0887A186ACBE6,
	qsScript_playGame2_mAF67BD74EAB249688ABC0D4CEADF6FA42BDB7481,
	qsScript_playGame3_m0287043EEE996B6719D6AB090597E88E5D046A39,
	qsScript__ctor_m17B2C3690D0FAA62B62EA87C97CB04792411AAC4,
	TapObject_Start_mA5FF65690991ADF170BDA001869EFBBB04DACB40,
	TapObject_Update_m8EFDF49DBF92542CC62EB880AACBBD474B2A27C3,
	TapObject_UpdatePlacementPose_m2B01C199B32209A905183C00218FAC7673CEEABF,
	TapObject__ctor_m03362C2439EF138FC96B734792F8B026D22642A6,
	maleMove_Start_mBB3812559561A577E6E7236CE4E92BCA2C71CE15,
	maleMove_Update_m8E71103445BF3B69264D55134AA99958E958839A,
	maleMove_OnTriggerEnter_m2EBC1BE40BD093F3050C9C49499CB99076A00DC7,
	maleMove_Wait_m126A6AA08CFF1A849029A9F9FF421A18A17795F0,
	maleMove_sadScene_m93C58647198AF096D17DA921BC74C709CCE486EC,
	maleMove_qScene_mA3135120CB6ED156A57915E60EEAE18AF32FEC90,
	maleMove_AddHealth_mC934C5FD23528F1A7A8CE90907FE5E4B1693DED8,
	maleMove_Heal_mD9CF89580C70E3F0156BC51E40D45C73245B617C,
	maleMove_Hurt_m7AE8D082443338702D5A1B1B83FFA5B212DF423B,
	maleMove__Heal_m1C9A7E40CCF6CA2ED2765157598784106320BD10,
	maleMove__Hurt_mEEFF23AE24B5E3E2569628CE7A1A74FF0BCB4DEF,
	maleMove_updateTextMine_mEC9B6972326A37B47ED546BE5F761B75647BC9C8,
	maleMove__ctor_m3DE00F53A727BF826E43BA20F5816BC2B0B047E0,
	OnHealthChangedDelegate__ctor_m3CF0F0556DE93BD37C78A360602129F13F6B1B5F,
	OnHealthChangedDelegate_Invoke_m6BFDB0DD8B980387D188C2F48B396C62F1BDA633,
	OnHealthChangedDelegate_BeginInvoke_mC27003B190C1E8B9F95574201198B56AD954F347,
	OnHealthChangedDelegate_EndInvoke_m6F94BF78376F371D033033DC7152B15E0F7F86A1,
	U3CWaitU3Ed__18__ctor_m7260307B18BDC5D64D4CF6803CDC97CA59C6403D,
	U3CWaitU3Ed__18_System_IDisposable_Dispose_mDE45B6C5AC3D81F040597555B42674E7705C47B1,
	U3CWaitU3Ed__18_MoveNext_m438946F3661F9CEE0D5A845D7E18BCAE14528206,
	U3CWaitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31769C4CA946C96D0D04FF74C528C93E6F7150EF,
	U3CWaitU3Ed__18_System_Collections_IEnumerator_Reset_m096B348A157986A020DF0164730877DFC427BE71,
	U3CWaitU3Ed__18_System_Collections_IEnumerator_get_Current_m8887D5CC0BC8989FFE458D54A4B0057126278A30,
};
static const int32_t s_InvokerIndices[119] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	289,
	289,
	23,
	4,
	677,
	677,
	677,
	289,
	289,
	23,
	23,
	23,
	23,
	23,
	32,
	32,
	31,
	31,
	23,
	23,
	677,
	677,
	1148,
	677,
	289,
	677,
	289,
	10,
	32,
	114,
	31,
	114,
	31,
	23,
	26,
	26,
	2137,
	23,
	1733,
	26,
	1713,
	23,
	677,
	289,
	23,
	26,
	26,
	2137,
	23,
	23,
	23,
	26,
	26,
	23,
	677,
	289,
	32,
	23,
	26,
	26,
	2137,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	289,
	289,
	23,
	23,
	169,
	23,
	102,
	23,
	113,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	119,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
