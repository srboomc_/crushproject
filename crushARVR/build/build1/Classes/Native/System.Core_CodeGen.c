﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000007 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000010 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000012 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000015 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000016 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000017 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000019 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001A System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001D System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000026 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000027 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000029 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000002A System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002B System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000002C System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000002D System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002F System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000030 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000031 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000032 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000033 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000034 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000035 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000036 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000037 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000038 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000039 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000003A System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000003B System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000003C System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000003D System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000003E System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000003F System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000040 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000041 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000042 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000043 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000044 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000045 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000046 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000047 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000048 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000004A System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000004B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000004C System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000004D System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000004E System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000004F System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000050 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000051 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000052 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000053 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000054 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000057 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000058 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000059 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000005A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000005B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000005C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000005D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000005E System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000005F System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000060 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000061 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000062 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000063 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000064 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000065 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000066 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[102] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[102] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[27] = 
{
	{ 0x02000004, { 43, 4 } },
	{ 0x02000005, { 47, 9 } },
	{ 0x02000006, { 56, 7 } },
	{ 0x02000007, { 63, 10 } },
	{ 0x02000008, { 73, 1 } },
	{ 0x0200000A, { 74, 3 } },
	{ 0x0200000B, { 79, 5 } },
	{ 0x0200000C, { 84, 7 } },
	{ 0x0200000D, { 91, 3 } },
	{ 0x0200000E, { 94, 7 } },
	{ 0x0200000F, { 101, 4 } },
	{ 0x02000010, { 105, 34 } },
	{ 0x02000012, { 139, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 5 } },
	{ 0x06000006, { 15, 2 } },
	{ 0x06000007, { 17, 1 } },
	{ 0x06000008, { 18, 3 } },
	{ 0x06000009, { 21, 2 } },
	{ 0x0600000A, { 23, 4 } },
	{ 0x0600000B, { 27, 3 } },
	{ 0x0600000C, { 30, 1 } },
	{ 0x0600000D, { 31, 3 } },
	{ 0x0600000E, { 34, 2 } },
	{ 0x0600000F, { 36, 2 } },
	{ 0x06000010, { 38, 5 } },
	{ 0x0600002E, { 77, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[141] = 
{
	{ (Il2CppRGCTXDataType)2, 20809 },
	{ (Il2CppRGCTXDataType)3, 19743 },
	{ (Il2CppRGCTXDataType)2, 20810 },
	{ (Il2CppRGCTXDataType)2, 20811 },
	{ (Il2CppRGCTXDataType)3, 19744 },
	{ (Il2CppRGCTXDataType)2, 20812 },
	{ (Il2CppRGCTXDataType)2, 20813 },
	{ (Il2CppRGCTXDataType)3, 19745 },
	{ (Il2CppRGCTXDataType)2, 20814 },
	{ (Il2CppRGCTXDataType)3, 19746 },
	{ (Il2CppRGCTXDataType)2, 20815 },
	{ (Il2CppRGCTXDataType)3, 19747 },
	{ (Il2CppRGCTXDataType)3, 19748 },
	{ (Il2CppRGCTXDataType)2, 16059 },
	{ (Il2CppRGCTXDataType)3, 19749 },
	{ (Il2CppRGCTXDataType)2, 20816 },
	{ (Il2CppRGCTXDataType)3, 19750 },
	{ (Il2CppRGCTXDataType)3, 19751 },
	{ (Il2CppRGCTXDataType)2, 20817 },
	{ (Il2CppRGCTXDataType)3, 19752 },
	{ (Il2CppRGCTXDataType)3, 19753 },
	{ (Il2CppRGCTXDataType)2, 16075 },
	{ (Il2CppRGCTXDataType)3, 19754 },
	{ (Il2CppRGCTXDataType)2, 20818 },
	{ (Il2CppRGCTXDataType)2, 20819 },
	{ (Il2CppRGCTXDataType)2, 16076 },
	{ (Il2CppRGCTXDataType)2, 20820 },
	{ (Il2CppRGCTXDataType)2, 16078 },
	{ (Il2CppRGCTXDataType)2, 20821 },
	{ (Il2CppRGCTXDataType)3, 19755 },
	{ (Il2CppRGCTXDataType)2, 16081 },
	{ (Il2CppRGCTXDataType)2, 16083 },
	{ (Il2CppRGCTXDataType)2, 20822 },
	{ (Il2CppRGCTXDataType)3, 19756 },
	{ (Il2CppRGCTXDataType)2, 20823 },
	{ (Il2CppRGCTXDataType)2, 16086 },
	{ (Il2CppRGCTXDataType)2, 20824 },
	{ (Il2CppRGCTXDataType)3, 19757 },
	{ (Il2CppRGCTXDataType)3, 19758 },
	{ (Il2CppRGCTXDataType)2, 20825 },
	{ (Il2CppRGCTXDataType)2, 16090 },
	{ (Il2CppRGCTXDataType)2, 20826 },
	{ (Il2CppRGCTXDataType)2, 16092 },
	{ (Il2CppRGCTXDataType)3, 19759 },
	{ (Il2CppRGCTXDataType)3, 19760 },
	{ (Il2CppRGCTXDataType)2, 16095 },
	{ (Il2CppRGCTXDataType)3, 19761 },
	{ (Il2CppRGCTXDataType)3, 19762 },
	{ (Il2CppRGCTXDataType)2, 16104 },
	{ (Il2CppRGCTXDataType)2, 20827 },
	{ (Il2CppRGCTXDataType)3, 19763 },
	{ (Il2CppRGCTXDataType)3, 19764 },
	{ (Il2CppRGCTXDataType)2, 16106 },
	{ (Il2CppRGCTXDataType)2, 20696 },
	{ (Il2CppRGCTXDataType)3, 19765 },
	{ (Il2CppRGCTXDataType)3, 19766 },
	{ (Il2CppRGCTXDataType)3, 19767 },
	{ (Il2CppRGCTXDataType)2, 16113 },
	{ (Il2CppRGCTXDataType)2, 20828 },
	{ (Il2CppRGCTXDataType)3, 19768 },
	{ (Il2CppRGCTXDataType)3, 19769 },
	{ (Il2CppRGCTXDataType)3, 19195 },
	{ (Il2CppRGCTXDataType)3, 19770 },
	{ (Il2CppRGCTXDataType)3, 19771 },
	{ (Il2CppRGCTXDataType)2, 16122 },
	{ (Il2CppRGCTXDataType)2, 20829 },
	{ (Il2CppRGCTXDataType)3, 19772 },
	{ (Il2CppRGCTXDataType)3, 19773 },
	{ (Il2CppRGCTXDataType)3, 19774 },
	{ (Il2CppRGCTXDataType)3, 19775 },
	{ (Il2CppRGCTXDataType)3, 19776 },
	{ (Il2CppRGCTXDataType)3, 19201 },
	{ (Il2CppRGCTXDataType)3, 19777 },
	{ (Il2CppRGCTXDataType)3, 19778 },
	{ (Il2CppRGCTXDataType)2, 20830 },
	{ (Il2CppRGCTXDataType)3, 19779 },
	{ (Il2CppRGCTXDataType)3, 19780 },
	{ (Il2CppRGCTXDataType)2, 20831 },
	{ (Il2CppRGCTXDataType)3, 19781 },
	{ (Il2CppRGCTXDataType)2, 20832 },
	{ (Il2CppRGCTXDataType)3, 19782 },
	{ (Il2CppRGCTXDataType)3, 19783 },
	{ (Il2CppRGCTXDataType)3, 19784 },
	{ (Il2CppRGCTXDataType)2, 16154 },
	{ (Il2CppRGCTXDataType)3, 19785 },
	{ (Il2CppRGCTXDataType)2, 16162 },
	{ (Il2CppRGCTXDataType)3, 19786 },
	{ (Il2CppRGCTXDataType)2, 20833 },
	{ (Il2CppRGCTXDataType)2, 20834 },
	{ (Il2CppRGCTXDataType)3, 19787 },
	{ (Il2CppRGCTXDataType)3, 19788 },
	{ (Il2CppRGCTXDataType)3, 19789 },
	{ (Il2CppRGCTXDataType)3, 19790 },
	{ (Il2CppRGCTXDataType)3, 19791 },
	{ (Il2CppRGCTXDataType)3, 19792 },
	{ (Il2CppRGCTXDataType)2, 16178 },
	{ (Il2CppRGCTXDataType)2, 20835 },
	{ (Il2CppRGCTXDataType)3, 19793 },
	{ (Il2CppRGCTXDataType)3, 19794 },
	{ (Il2CppRGCTXDataType)2, 16182 },
	{ (Il2CppRGCTXDataType)3, 19795 },
	{ (Il2CppRGCTXDataType)2, 20836 },
	{ (Il2CppRGCTXDataType)2, 16192 },
	{ (Il2CppRGCTXDataType)2, 16190 },
	{ (Il2CppRGCTXDataType)2, 20837 },
	{ (Il2CppRGCTXDataType)3, 19796 },
	{ (Il2CppRGCTXDataType)2, 20838 },
	{ (Il2CppRGCTXDataType)3, 19797 },
	{ (Il2CppRGCTXDataType)3, 19798 },
	{ (Il2CppRGCTXDataType)2, 16199 },
	{ (Il2CppRGCTXDataType)3, 19799 },
	{ (Il2CppRGCTXDataType)2, 16199 },
	{ (Il2CppRGCTXDataType)3, 19800 },
	{ (Il2CppRGCTXDataType)2, 16216 },
	{ (Il2CppRGCTXDataType)3, 19801 },
	{ (Il2CppRGCTXDataType)3, 19802 },
	{ (Il2CppRGCTXDataType)3, 19803 },
	{ (Il2CppRGCTXDataType)2, 20839 },
	{ (Il2CppRGCTXDataType)3, 19804 },
	{ (Il2CppRGCTXDataType)3, 19805 },
	{ (Il2CppRGCTXDataType)3, 19806 },
	{ (Il2CppRGCTXDataType)2, 16196 },
	{ (Il2CppRGCTXDataType)3, 19807 },
	{ (Il2CppRGCTXDataType)3, 19808 },
	{ (Il2CppRGCTXDataType)2, 16201 },
	{ (Il2CppRGCTXDataType)3, 19809 },
	{ (Il2CppRGCTXDataType)1, 20840 },
	{ (Il2CppRGCTXDataType)2, 16200 },
	{ (Il2CppRGCTXDataType)3, 19810 },
	{ (Il2CppRGCTXDataType)1, 16200 },
	{ (Il2CppRGCTXDataType)1, 16196 },
	{ (Il2CppRGCTXDataType)2, 20839 },
	{ (Il2CppRGCTXDataType)2, 16200 },
	{ (Il2CppRGCTXDataType)2, 16198 },
	{ (Il2CppRGCTXDataType)2, 16202 },
	{ (Il2CppRGCTXDataType)3, 19811 },
	{ (Il2CppRGCTXDataType)3, 19812 },
	{ (Il2CppRGCTXDataType)3, 19813 },
	{ (Il2CppRGCTXDataType)2, 16197 },
	{ (Il2CppRGCTXDataType)3, 19814 },
	{ (Il2CppRGCTXDataType)2, 16212 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	102,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	27,
	s_rgctxIndices,
	141,
	s_rgctxValues,
	NULL,
};
